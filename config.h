/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                       /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int center_width = 1000;             /* default width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = "JetBrains Mono Nerd Font SemiBold 10";

static char bg_norm[] = "#1a1b26";
static char fg_norm[] = "#cac0f5";

static char fg_sel[] = "#1a1b26";
static char bg_sel[] = "#a9b1d6";

static char fg_out[] = "#1a1b26";
static char bg_out[] = "#bb9af7";

static char fg_sel_high[] = "#cac0f5";
static char bg_sel_high[] = "#1a1b26";

static char fg_norm_high[] = "#000000";
static char bg_norm_high[] = "#a9b1d6";

static char border_col[] = "#a9b1d6";

static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 25;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
