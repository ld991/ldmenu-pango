# ldmenu-pango

`ldmenu-pango` is my build of `dmenu` that uses Pango and Cairo. It is built on top of or is a heavily modified version of [this dmenu build](https://git.speedie.site/speedie/dmenu-pango-cairo).

### But what is `dmenu`?
[`dmenu`](https://tools.suckless.org/dmenu) is a program for X11 that can take a string with multiple lines and turn that input string into a dynamic menu.

## Screenshot
![](screenshots/preview.png)

## Requirements

In order to build dmenu, you need the Xlib header files.
For `ldmenu-pango`, also install the font specified in `config.h`.


## Installation

```sh
git clone https://gitlab.com/ld991/ldmenu-pango.git
cd ldmenu-pango/
sudo make clean install
```

## Features

Read the fine manual.

```sh
man dmenu
```
