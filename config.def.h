/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                       /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int center_width = 800;              /* default width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = "Monospace 10";
static char bg_norm[] = "#222222";
static char fg_norm[] = "#bbbbbb";
static char bg_sel[] = "#005577";
static char fg_sel[] = "#eeeeee";
static char bg_out[] = "#000000";
static char fg_out[] = "#00ffff";
static char bg_sel_high[] = "#000000"
static char fg_sel_high[] = "#ffffff"
static char bg_norm_high[] = "#00ffff"
static char fg_norm_high[] = "#000000"
static char border_col[] = "#005577";
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
